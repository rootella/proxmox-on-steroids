# Proxmox on steroids

Optimization and Performance Tweaks for Proxmox VE

# Resources
https://gist.github.com/sergey-dryabzhinsky/bcc1a15cb7d06f3d4606823fcc834824
https://martin.heiland.io/2018/02/23/zfs-tuning/
https://icesquare.com/wordpress/how-to-improve-zfs-performance/